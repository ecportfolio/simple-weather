import React from "react";

interface IState {
    response: string;
}

interface IProps {}

export default class TestButton extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            response: ""
        }

        this.sendRequest = this.sendRequest.bind(this);
    }
    public render() {
        return (
            <>
                <button onClick={this.sendRequest}>
                    Send request!
                </button>
                {this.state.response && <p>Response received: {this.state.response}</p>}
            </>

        );
    }

    private async sendRequest() {
        fetch("http://localhost:8080/test")
            .then((response) => response.json())
            .then((data) => this.setState({response: data.text}))
            // .then((data) => this.setState({response: data.text}));
    }
}