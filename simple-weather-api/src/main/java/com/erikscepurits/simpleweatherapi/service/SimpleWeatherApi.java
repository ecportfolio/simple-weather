package com.erikscepurits.simpleweatherapi.service;

import com.erikscepurits.openmeteointegration.dto.CurrentWeatherDto;
import com.erikscepurits.openmeteointegration.service.OpenMeteoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
public class SimpleWeatherApi {

    private final OpenMeteoService openMeteoService;

    @Autowired
    public SimpleWeatherApi(OpenMeteoService openMeteoService) {
        this.openMeteoService = openMeteoService;
    }

    @GetMapping("/currentWeather")
    public CurrentWeatherDto testEndpoint() {
        log.info("Request received!");
        CurrentWeatherDto currentWeather = openMeteoService.getCurrentWeather();
        return currentWeather;
    }
}
