package com.erikscepurits.simpleweatherapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({
        "com.erikscepurits.simpleweatherapi",
        "com.erikscepurits.openmeteointegration"
})
@SpringBootApplication
public class SimpleWeatherApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleWeatherApiApplication.class, args);
    }

}
