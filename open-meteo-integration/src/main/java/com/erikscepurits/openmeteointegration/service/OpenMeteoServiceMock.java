package com.erikscepurits.openmeteointegration.service;

import com.erikscepurits.openmeteointegration.dto.CurrentWeatherDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Service
@Profile("open-meteo-mock")
@Slf4j
public class OpenMeteoServiceMock implements OpenMeteoService {

    private final Resource mockApiResponse;

    public OpenMeteoServiceMock(@Value("${mock.api.response.current-weather.path}") Resource mockApiResponse) {
        this.mockApiResponse = mockApiResponse;
    }

    @Override
    public CurrentWeatherDto getCurrentWeather() {
        ExchangeFunction exchange = request -> buildExchangeFunction();

        WebClient client = WebClient.builder().exchangeFunction(exchange).build();
        WebClient.RequestBodyUriSpec bodySpec = client.method(HttpMethod.GET);
        WebClient.ResponseSpec response = bodySpec.retrieve();

        // TODO: investigate reactive programming concepts
        return response.bodyToMono(CurrentWeatherDto.class).block();
    }

    // TODO: rework error handling
    private Mono<ClientResponse> buildExchangeFunction() {
        try {
            ClientResponse mockResponse = ClientResponse.create(HttpStatus.OK)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .body(mockApiResponse.getContentAsString(StandardCharsets.UTF_8))
                    .build();
            return Mono.just(mockResponse);
        } catch (IOException exception) {
            log.error("Mock response file was not found", exception);
            return Mono.just(ClientResponse.create(HttpStatus.INTERNAL_SERVER_ERROR).build());
        }
    }
}
