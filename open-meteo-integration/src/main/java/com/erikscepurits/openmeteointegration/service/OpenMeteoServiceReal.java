package com.erikscepurits.openmeteointegration.service;

import com.erikscepurits.openmeteointegration.dto.CurrentWeatherDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@Profile("open-meteo-real")
@Slf4j
public class OpenMeteoServiceReal implements OpenMeteoService {

    @Override
    public CurrentWeatherDto getCurrentWeather() {
        String urlString = "https://api.open-meteo.com/v1/forecast?latitude=56.946&longitude=24.1059&current=temperature_2m&wind_speed_unit=ms&timezone=auto&forecast_days=1";

        WebClient client = WebClient.create(urlString);
        WebClient.RequestBodyUriSpec bodySpec = client.method(HttpMethod.GET);
        WebClient.ResponseSpec response = bodySpec.retrieve();

        // TODO: investigate reactive programming concepts
        return response.bodyToMono(CurrentWeatherDto.class).block();
    }
}
