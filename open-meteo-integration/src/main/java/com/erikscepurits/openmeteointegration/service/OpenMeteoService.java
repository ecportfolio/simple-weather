package com.erikscepurits.openmeteointegration.service;


import com.erikscepurits.openmeteointegration.dto.CurrentWeatherDto;

public interface OpenMeteoService {
    CurrentWeatherDto getCurrentWeather();
}
