package com.erikscepurits.openmeteointegration;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenMeteoIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenMeteoIntegrationApplication.class, args);
    }

}
