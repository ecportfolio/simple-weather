package com.erikscepurits.openmeteointegration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrentWeatherDto {

    @JsonProperty("latitude")
    private double latitude;

    @JsonProperty("longitude")
    private double longitude;

    @JsonProperty("generationtime_ms")
    private double generationTimeMs;

    @JsonProperty("utc_offset_seconds")
    private int utcOffsetSeconds;

    @JsonProperty("timezone")
    private String timezone;

    @JsonProperty("timezone_abbreviation")
    private String timezoneAbbreviation;

    @JsonProperty("elevation")
    private double elevation;

    @JsonProperty("current_units")
    private Units currentUnits;

    @JsonProperty("current")
    private Current current;

    @Getter
    @Setter
    public static class Units {
        @JsonProperty("time")
        private String time;

        @JsonProperty("interval")
        private String interval;

        @JsonProperty("temperature_2m")
        private String temperature2m;
    }

    @Getter
    @Setter
    public static class Current {
        @JsonProperty("time")
        private String time;

        @JsonProperty("interval")
        private double interval;

        @JsonProperty("temperature_2m")
        private double temperature2m;
    }

}

