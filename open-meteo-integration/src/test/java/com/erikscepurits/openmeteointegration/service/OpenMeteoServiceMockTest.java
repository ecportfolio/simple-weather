package com.erikscepurits.openmeteointegration.service;

import com.erikscepurits.openmeteointegration.dto.CurrentWeatherDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class OpenMeteoServiceMockTest {

    private OpenMeteoService openMeteoServiceMock;

    private Resource mockApiResponse;

    @Test
    void testCurrentWeatherMapping() throws IOException {
        // Arrange
        String response = new String(Files.readAllBytes(Paths.get("src/test/resources/mock/responses/currentWeather.json")));

        mockApiResponse = Mockito.mock(Resource.class);
        Mockito.when(mockApiResponse.getContentAsString(StandardCharsets.UTF_8)).thenReturn(response);

        openMeteoServiceMock = new OpenMeteoServiceMock(mockApiResponse);

        // Act
        CurrentWeatherDto currentWeatherDto = openMeteoServiceMock.getCurrentWeather();

        // Assert
        Assertions.assertNotNull(currentWeatherDto);
        Assertions.assertEquals(56.944096, currentWeatherDto.getLatitude());
        Assertions.assertEquals(24.109085, currentWeatherDto.getLongitude());
        Assertions.assertEquals(0.010967254638671875, currentWeatherDto.getGenerationTimeMs());
        Assertions.assertEquals(7200, currentWeatherDto.getUtcOffsetSeconds());
        Assertions.assertEquals("Europe/Riga", currentWeatherDto.getTimezone());
        Assertions.assertEquals("EET", currentWeatherDto.getTimezoneAbbreviation());
        Assertions.assertEquals(6.0, currentWeatherDto.getElevation());

        CurrentWeatherDto.Units currentUnits = currentWeatherDto.getCurrentUnits();

        Assertions.assertNotNull(currentUnits);
        Assertions.assertEquals("iso8601", currentUnits.getTime());
        Assertions.assertEquals("seconds", currentUnits.getInterval());
        Assertions.assertEquals("°C", currentUnits.getTemperature2m());

        CurrentWeatherDto.Current current = currentWeatherDto.getCurrent();

        Assertions.assertNotNull(current);
        Assertions.assertEquals("2023-11-11T21:00", current.getTime());
        Assertions.assertEquals(900, current.getInterval());
        Assertions.assertEquals(10.5, current.getTemperature2m());
    }
}
